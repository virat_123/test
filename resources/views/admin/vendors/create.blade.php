@extends('admin.layouts.app')
@section('title', 'Vendors')
@section('sub_title', 'Create Vendor')

@section('content')
<div class="row">
  <div class="col-md-12">
      <div class="box box-primary">
        {!! Form::open(['route' => ['vendors.store'],'files' => true]) !!}
        <div class="box-body">
          {{ csrf_field() }}
          <div class="row">
            <div class="form-group">
              <div class="col-md-10">
                {!! Form::label('category', 'Category', ['class' => 'required']) !!}
                 <select name="category" id="parent_category" onchange="delete_sub_div_content()" class="form-control">
                  <option value="">Select Category</option>
                  @foreach($categories as $s):
                  <option value="{{$s->id}}">{{$s->name}}</option>
                  @endforeach;
                </select>
              </div>
                <div class="col-md-2 col-sm-4 col-lg-1">
                    <br>
                  <button type="button" id="parent_category_button" onclick="get_subcategory()" class="btn bg-blue btn-flat"><i class="fa fa-plus">Add Sub Category</i></button>
                </div>
            </div>
          </div>

          <div class="row">
              <div class="form-group" id="subcategories" style="display:none">
             </div>
             <br/>
          </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  {!! Form::label('name', 'Name', ['class' => 'required']) !!}
                  {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  {!! Form::label('email', 'Email') !!}
                  {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  {!! Form::label('phone', 'Phone No.') !!}
                  {!! Form::text('phone', null, array('placeholder' => 'Phone no.','class' => 'form-control')) !!}
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  {!! Form::label('password', 'Password') !!}
                  {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  {!! Form::label('confirm password', 'Confirm Password') !!}
                  {!! Form::password('confirm_password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  {!! Form::label('state', 'State') !!}
                  <select name="state" id="state" class="form-control">
                    <option value="">Select State</option>
                    @foreach($states as $s):
                    <option value="{{$s->id}}">{{$s->name}}</option>
                    @endforeach;
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  {!! Form::label('city', 'City') !!}
                  <select name="city" id="city" class="form-control">
                    <option value="">Select City</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                {!! Form::label('pincode', 'Pin Code') !!}
                {!! Form::text('pincode', null, array('placeholder' => 'Pin Code','class' => 'form-control')) !!}
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                {!! Form::label('address', 'Address') !!}
                {!! Form::textarea('address', null, array('placeholder' => 'Address','class' => 'form-control', 'rows' => 3, 'cols' => 40)) !!}
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                {!! Form::label('latitude', 'Latitude') !!}
                {!! Form::text('latitude', null, array('placeholder' => 'Latitude','class' => 'form-control','id'=>'latitude')) !!}
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                {!! Form::label('longitude', 'Longitude') !!}
                {!! Form::text('longitude', null, array('placeholder' => 'Longitude','class' => 'form-control','id'=>'longitude')) !!}
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                {!! Form::label('percetage', 'Percentage(%)') !!}
                {!! Form::text('percentage', null, array('placeholder' => 'Percentage','class' => 'form-control','id'=>'percentage')) !!}
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                {!! Form::label('vendor_icon ', 'Vendor Icon') !!}
               {!! Form::file('vendor_icon', null, array('placeholder' => '','class' => 'form-control','id'=>'vendor_icon')) !!}
               </div>
              </div>
            </div>
          <div class="row">
     
            <div class="col-sm-12">
             
            </div>
          </div>

            <div class="col-md-12  text-center">
              <button type="submit" class="btn bg-blue btn-flat ajax-submit">Save</button>
              <a href="" class="btn bg-red btn-flat">Cancel</a>
            </div>
        </div>
      </form>
      </div>
  </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
  function get_subcategory()
       {

        cat_id=$("#parent_category").val();
        if(cat_id=="")
        {
          swal("Please select category");
          return;
        }
        count=$("#subcategories .sub_cat").length;
        var url = '{{ route("category.getsubcategory",array(":id",":count")) }}';
        url = url.replace(':id',cat_id);
        url=url.replace(':count',count);
        $.ajax({
          type: "GET",
          url: url,
          success: function(data) {
        $('#subcategories').append(data);
        $('#subcategories').show(1000);
        $('#parent_category_button').hide(1000);
        // $('#unit_topic').html(data);
          }
  });
       }

       function delete_sub_div_content()
       {
        $('#subcategories').html('');
        $('#subcategories').hide(1000);
         $('#parent_category_button').show(1000);
       }

      function get_sub_subcategory(id)
       {
       cat_id=$("#sub_category"+id).val();
       count=$("#subcategories .sub_cat").length;
      
       var url = '{{ route("category.getsubsubcategory",array(":id",":count")) }}';
       url = url.replace(':id',cat_id);
       url=url.replace(':count',count);

       $.ajax({
         type: "GET",
         url: url,
         success: function(data) {

        $('#subcategories').append(data);
        $('#sub_category_button'+id).hide(1000);
        // $('#unit_topic').html(data);
          }
        });

       }
      function delete_subdiv_contetnt(id)
       {
         newid=parseInt(id) + 1;

        $('#subCategory'+newid).html('');
        $("#subCategory"+newid).remove();
        $('#subCategory'+newid).hide(1000);
        $('#sub_category_button'+id).show(1000);
       }
$(document).on("change","#state",function(){
    var stateId=$(this).val();
    if(stateId == "")
    {
      swal("Please Select Contry");
    }
    if(stateId)
    {
      var url = '{{ route("get.city", ":id") }}';
      url = url.replace(':id',stateId);

          $.ajax({
                url: url,
                type:"GET",
                dataType:"json",
                success:function(data) {

                    $('select[name="city"]').empty();
                    $.each(data, function(key, value){

                        $('select[name="city"]').append('<option value="'+ key +'">' + value + '</option>');
                    });
                },
                complete: function(){
                    //$('#loader').css("visibility", "hidden");
                }
            });
        }
        else
        {
            $('select[name="city"]').empty();
        }
})
</script>
<script>
$(document).ready(function(){
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showLocation);
    } else {
        $('#location').html('Geolocation is not supported by this browser.');
    }
});
function showLocation(position) {

    var latitude = position.coords.latitude;
	var longitude = position.coords.longitude;
  $("#latitude").val(latitude);
  $("#longitude").val(longitude);

	// $.ajax({
	// 	type:'POST',
	// 	url:'getLocation.php',
	// 	data:'latitude='+latitude+'&longitude='+longitude,
	// 	success:function(msg){
  //           if(msg){
  //              $("#location").html(msg);
  //           }else{
  //               $("#location").html('Not Available');
  //           }
	// 	}
	// });
}
</script>
<script>
$(document).on('submit','form',function(e){

e.preventDefault();
        cat_id=$("#parent_category").val();
        if(cat_id=="")
        {
          swal("Please select category");
          return;
        }
              var me = $(this);
               var fd=new FormData($(this)[0]);
       $.ajax({

           url: me.attr('action'),

           type: 'post',

           data: fd,

          dataType: 'json',
           contentType: false,
           processData: false,

           success: function(response){

               if(response.success == true)
               {
                swal("Done","Vendor added successfully","success");
               //alert('category added successfully');
               
               setTimeout(function () {
    location.reload();
  }, 2000);

                $("#form_error").hide();
            }

            else{

               $.each(response.responseJSON.errors, function(key, value) {

                     var element = $('#' + key);
                       element.closest('div.form-group')

                       .removeClass('.has-error')

                       .addClass(value.length > 0 ? 'has-error':'has-success' )

                       .find('.text-danger')

                       .remove();
                        element.after(value);

               });
           }
       },
       error:function(response){
       
         errors=console.log(response.responseJSON);
         $.each(response.responseJSON.errors, function(key, value) {

               var element = $('#' + key);
                 element.closest('div.form-group')

                 .removeClass('.has-error')

                 .addClass(value.length > 0 ? 'has-error':'has-success' )

                 .find('.text-danger')

                 .remove();
                  element.after('<p class="text text-danger">'+value+'</p>');

         });
       }
   });
   });

</script>
@endpush
