@extends('admin.layouts.app')

@section('title', 'Transactions')
@section('sub_title', '')
@push('styles')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
@endpush
@section('content')

<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h2 class="box-title">Transaction List</h2>

                    <a href="{{ route('transactions.create') }}" class="btn bg-purple btn-flat pull-right"><i class="fa fa-plus"></i> Create New</a>

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-striped" id="users_table">
                        <thead>
                            <tr>
                                <th>User Name</th>
                                <th>Product Name</th>
                                <th>Amount</th>
                               <!--  <th>Assigned Nodes</th> -->
                                <th>Vendor Name</th>
                                <th>Total Amount</th>
                               
                            </tr>
                        </thead>        
                        <tbody>
                            <?php 
                            $amount=0;
                            $totalnodes=0
                             ?>
                            @foreach($transactions as $t)
                                <?php
                                $nodes=$t->amount/1000;
                                $amount+=$t->amount;
                                $totalnodes+=$nodes;
                                ?>
                                <tr>
                                    <td>{{$t->users->name}}</td>
                                    <td>{{$t->product_name}}</td>
                                    <td>{{$t->amount}}</td>
                                     <!-- <td>{{$nodes}}</td> -->
                                    <td>{{$t->vendors->name}}</td>
                                    <td ><i class="fa fa-inr"></i> {{$t->amount}}</td>
                                 
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="3">
                                       Total
                                    </td>
                               <!--      <td>
                                        {{$totalnodes}}
                                    </td> -->
                                    <td>
                                    </td>
                                    <td>
                                      <i class="fa fa-inr"></i>{{$amount}}
                                    </td>
                                    
                                </tr>
                             
                                
                              
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@endsection
@push('scripts')
<!-- DataTables -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
	    $('#users_table').DataTable({    
     });
});

$(document).on('click','.delete',function(){
  var _token = $('input[name="_token"]').val();
    $id=$(this).attr('id');
    $action=$(this).data('src');
   swal({
  title: "Are you sure?",
  text: "User will be deleted!",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Yes, delete it!",
  closeOnConfirm: false
},
function(){

   $.ajax({

            url: $action,
            type: "DELETE",
            data:{
                "_token": "{{ csrf_token() }}",
            },
            success: function (){
                swal("Deleted!", "User has been deleted.", "success");
                location.reload();
            }
});
});
});


</script>
@endpush
