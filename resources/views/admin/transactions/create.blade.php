@extends('admin.layouts.app')

@section('title', 'Transactions')
@section('sub_title', 'Create Transaction')

@section('content')
	<div class="row">
	    <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Create Transactions</h3>
                </div>
                @if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
                <!-- /.box-header -->
	{!! Form::open(['route' => ['transactions.store']]) !!}
    	<div class="row">
    		<div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('product_name', 'Product Name', ['class' => 'required']) !!}
                    {!! Form::text('product_name', null, array('placeholder' => 'Product Name','class' => 'form-control')) !!}
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <strong>Price:</strong>
                    {!! Form::text('price', null, array('placeholder' => 'Price','class' => 'form-control')) !!}
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <strong>Vendor:</strong>
                    <select name="vendor"  class="form-control">
                      @foreach($vendors as $v)
                      <option value="{{ $v['id'] }}">{{$v['name']}}</option>
                      @endforeach
                    </select>
                </div>
            </div>

        </div>

        <div class="box-footer text-center">
             <button type="submit" class="btn bg-blue btn-flat ajax-submit">Save</button>
            <a href="{{ route('transactions.index') }}" class="btn bg-red btn-flat">Cancel</a>
        </div>
	{!! Form::close() !!}
    </div>
            <!-- /.box -->
        </div>
        <!--/.col (left) -->
    </div>

@endsection

@push('scripts')



@endpush
