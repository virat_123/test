@extends('admin.layouts.app')

@section('title', 'User Transactions')
@section('sub_title', '')
@push('styles')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
@endpush
@section('content')

<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h2 class="box-title">Transaction List</h2>

                    <a href="{{ route('user.create') }}" class="btn bg-purple btn-flat pull-right"><i class="fa fa-plus"></i> Create New</a>

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-striped" id="users_table">
                        <thead>
                            <tr>
                                <th>User Name</th>
                                <th>Product Name</th>
                                <th>Amount</th>
                            <!--     <th>Assigned Nodes</th> -->
                                <th>Vendor Name</th>
                                <th>Total Amount</th>
                               
                            </tr>
                        </thead>        
                        <tbody>
                            <?php 
                            $amount=0;
                            $totalnodes=0
                             ?>
                            @foreach($user_transaction as $t)
                                <?php
                                $nodes=$t->amount/1000;
                                $amount+=$t->amount;
                                $totalnodes+=$nodes;
                                ?>
                                <tr>
                                    <td>{{$t->users->name}}</td>
                                    <td>{{$t->product_name}}</td>
                                    <td>{{$t->amount}}</td>
                                     <!-- <td>{{$nodes}}</td> -->
                                    <td>{{$t->vendors->name}}</td>
                                    <td ><i class="fa fa-inr"></i> {{$t->amount}}</td>
                                 
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="3">
                                       Total
                                    </td>
                               <!--      <td>
                                        {{$totalnodes}}
                                    </td> -->
                                    <td>
                                    </td>
                                    <td>
                                      <i class="fa fa-inr"></i>{{$amount}}
                                    </td>
                                    
                                </tr>
                             
                                
                              
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h2 class="box-title">User Earnings</h2>

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-striped" id="users_table">
                        <thead>
                            <tr>
                                <th>User Name</th>
                                <th>Product Name</th>
                                <th>Amount</th>
                            <!--     <th>Assigned Nodes</th> -->
                                <th>Vendor Name</th>
                                <th>Total Amount</th>
                               
                            </tr>
                        </thead>        
                        <tbody>
                            <?php 
                            $amount=0;
                            $totalnodes=0
                             ?>
                            @foreach($user_transaction as $t)
                                <?php
                                $nodes=$t->amount/1000;
                                $amount+=$t->amount;
                                $totalnodes+=$nodes;
                                ?>
                                <tr>
                                    <td>{{$t->users->name}}</td>
                                    <td>{{$t->product_name}}</td>
                                    <td>{{$t->amount}}</td>
                                     <!-- <td>{{$nodes}}</td> -->
                                    <td>{{$t->vendors->name}}</td>
                                    <td ><i class="fa fa-inr"></i> {{$t->amount}}</td>
                                 
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="3">
                                       Total
                                    </td>
                               <!--      <td>
                                        {{$totalnodes}}
                                    </td> -->
                                    <td>
                                    </td>
                                    <td>
                                      <i class="fa fa-inr"></i>{{$amount}}
                                    </td>
                                    
                                </tr>
                             
                                
                              
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>


@endsection
@push('scripts')
@endpush('scripts')