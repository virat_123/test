@extends('admin.layouts.app')

@section('title', 'Users')
@section('sub_title', 'Create User')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create Users</h3>
        </div>

        @if(count($errors) > 0)
          <div class="alert alert-danger">
              <strong>Whoops!</strong> There were some problems with your input.<br><br>
              <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
              </ul>
          </div>
      @endif
    </div>

    {!! Form::open(['route' => ['user.store']]) !!}
    	<div class="row">
        	<div class="col-md-12">
            <div class="form-group">
                {!! Form::label('Name', 'Name', ['class' => 'required']) !!}
                {!! Form::text('name', null, array('placeholder' => 'First Name','class' => 'form-control')) !!}
            </div>
          </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <strong>Email:</strong>
            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <strong>Phone:</strong>
            {!! Form::text('phone', null, array('placeholder' => 'phone','class' => 'form-control')) !!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <strong>Password:</strong>
            {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <strong>Confirm Password:</strong>
            {!! Form::password('c_password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <strong>State:</strong>
            <select name="state" id="State" class="form-control">
              <option>Select State</option>
              @foreach($states as $s):
              <option value="{{$s->id}}">{{$s->name}}</option>
              @endforeach;
            </select>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <strong>City:</strong>
            <select name="city" class="form-control">
              <option>Select City</option>
            </select>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
          <strong>Pin Code:</strong>
          {!! Form::text('pincode', null, array('placeholder' => 'Pin Code','class' => 'form-control')) !!}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
          <strong>Address:</strong>
          {!! Form::textarea('address', null, array('placeholder' => 'Address','class' => 'form-control', 'rows' => 3, 'cols' => 40)) !!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            {!! Form::label('Status', 'Status: ', ['class' => 'required']) !!}
            <label for="status-active"><input id="status-active" type="radio" name="status" value="1" checked> Active</label>&nbsp;
            <label for="status-inactive"><input id="status-inactive" type="radio" name="status" value="0"> InActive</label>
          </div>
        </div>
      </div>


      <div class="box-footer text-center">
          <button type="submit" class="btn bg-blue btn-flat ajax-submit">Save</button>
          <a href="{{ route('user.index') }}" class="btn bg-red btn-flat">Cancel</a>
      </div>

  </div>
</div>

@endsection
@push('scripts')
<script type="text/javascript">
$(document).on("change","#State",function(){
    var stateId=$(this).val();
    if(stateId == "")
    {
      swal("Please Select Contry");
    }
    if(stateId)
    {

      var url = '{{ route("get.city", ":id") }}';
      url = url.replace(':id',stateId);

          $.ajax({
                url: url,
                type:"GET",
                dataType:"json",
                success:function(data) {

                    $('select[name="city"]').empty();
                    $.each(data, function(key, value){

                        $('select[name="city"]').append('<option value="'+ key +'">' + value + '</option>');
                    });
                },
                complete: function(){
                    //$('#loader').css("visibility", "hidden");
                }
            });
        }
        else
        {
            $('select[name="city"]').empty();
        }

})
</script>
@endpush
