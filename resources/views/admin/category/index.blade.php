@extends('admin.layouts.app')
@section('title', 'Categories')
@section('sub_title', 'Create Category')
@section('content')
<div class="row" align="center">
                        <div class="col-md-12">
                            <div class="white-box">
                                <h3 class="box-title">View Categories</h3>
                                 <p class="text-danger">Note:Drag And Drop Category To Make It Parent Or Child Category.</p>
                               
                                    <div class="nestable nestable-lists">
	                                    <div class="dd" id="nestable">
	                                        <?php echo $data['menuview']; ?>
	                                    </div>
                                   </div>
                          

                            </div>
                        </div>
                    </div>


                    <!-- Modal Categories -->
                                <div id="category-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title">Categories</h4> 
                                            </div>
                                            <form id="update_category" class="form-material form-horizontal" action="<?php //echo base_url('Admin/Inventory/Category/update_category_name'); ?>" method="post">
                                                <div class="modal-body">
                                                    <div class="form-group" id="selectedCategory" style="display: non">
                                                        <div class="col-md-12 col-sm-12 col-lg-12">
                                                            <input type="text" name="selected_category" id="selected_category" class="form-control" placeholder="Category Name" >
                                                        </div>
                                                    </div>
                                                   
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button type="submit"  class="btn btn-success waves-effect waves-light">Save</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Modal Categories -->
@endsection
@push('scripts')
<script> 
    $('#nestable').nestable();
</script>
        <script>
        $('#nestable').on('change', function() {
        
        $list=$('#nestable').nestable('serialize');
    	$json_list=JSON.stringify($list);
    	//alert($json_list);
    	 $.ajax({
                    url: "{{ route('category.updateorder') }}",
                    type: "get",
                    data: {Category_list: $json_list },
                    dataType: 'json',
                    success: function (response)
                    {
                        if ($.trim(response.success) == "true")
                        {
                           
                            //location.reload();
                           
                        } else {
                            alert("Update Failed");
                        }
                    }
                });
    
    /* on change event */
});
        </script>
        <script>
        function delete1(id) {
       
         if (confirm("Do you want to add sub category?") == true) 
	 {
                //alert("hii");
                $.ajax({
                    url: "<?php //echo base_url('Admin/Inventory/Category/delete_category?id_d='); ?>" + id,
                    type: "POST",
                    dataType: 'json',
                    success:function (response)
                    {
                        if($.trim(response.success) == "true")
                        {
                           alert("Category deleted Successfully.");
                           location.reload();
                       }
                   }
               });

            }
        
            else
            {
            
            }
            }
        </script>
        <script>
        
        function update_category_modal(id)
		{
			$.ajax({
				type: "POST",
				url: "<?php //echo base_url();?>Admin/Category/get_selected_category/"+id,
				success: function(data) {
           // data is ur summary
           
           	//$('#modal_content').html(data);
 		$("#selected_category").val(data);
 		
          	 $('#category-modal').modal('show');

       		}
   		});
		}
        </script>
        <script>
        $(document).ready(function(){
               $('#update_category').submit(function(e){

                e.preventDefault();

                var me = $(this);
                var fd = new FormData($(this)[0]);

                    var other_data = me.serializeArray();
                    $.each(other_data, function (key, input) {
                        fd.append(input.name, input.value);
                    });

        // perform ajax

        $.ajax({

              url: me.attr('action'),

              type: 'post',

              data: fd,

              dataType: 'json',
              processData: false,
              contentType: false,

            success: function(response){

                if(response.success == true)
                {

                alert('Category Updated successfully');
              location.reload();
                
                 $("#form_error").hide();
             }

             else{

                $.each(response.messages, function(key, value) {

                      var element = $('#' + key);
                        element.closest('div.form-group')

                        .removeClass('.has-error')

                        .addClass(value.length > 0 ? 'has-error':'has-success' )

                        .find('.text-danger')

                        .remove();
                         element.after(value);

                });
            }
        }
    });
    });
           });
        </script>
@endpush