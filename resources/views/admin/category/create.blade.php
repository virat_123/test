@extends('admin.layouts.app')

@section('title', 'Categories')
@section('sub_title', 'Create Category')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      {!! Form::open(['route' => ['category.store'],'files' => true]) !!}
  <div class="box-body">
          {{ csrf_field() }}
      <div class="row">
        <label class="col-sm-12">Category</label>
        <div class="col-md-10">
            <div class="form-group">
              {!! Form::text('category_name', null, array('placeholder' => 'First Name','class' => 'form-control','id'=>'category_name')) !!}
            </div>
        </div>
        <div class="col-md-2">
          <button type="button" onclick="add_parentcategory()"  class="btn bg-blue btn-flat show-parentcat"><i class="fa fa-plus"></i></button>
          <button type="button" onclick="hide_parentcategory()" style="display:none" class="btn bg-blue btn-flat hide-parentcat"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      
      <div class="row">
        <label class="col-sm-12">Category Icon</label>
        <div class="col-sm-12">
          <div class="form-group">
          {!! Form::file('category_icon', null, array('placeholder' => '','class' => 'form-control','id'=>'category_icon')) !!}
        </div>
        </div>
      </div>

    <div class="row" id="parentCategory" style="Display:none">
      @if(!empty($parent_categories))
      <label class="col-sm-12">Parent Category</label>
        <div class="col-md-10">
          <div class="form-group">
           <select name="parent_category" onchange="delete_sub_div_content()" id="parent_category" class="form-control">
             <option value="">Select Parent Category</option>
             @foreach($parent_categories as $p)
                <option value="{{$p->id}}">{{$p->name}}</option>
             @endforeach
           </select>
         </div>
       </div>
         <div class="col-md-2 col-sm-4 col-lg-1">
           <button type="button" id="parent_category_button" onclick="get_subcategory()" class="btn bg-blue btn-flat"><i class="fa fa-plus"></i>
          </div>
          @else
          <p class="text-danger">No parent Category Available</p>
          @endif
        </div>



        <div class="row">
        <div class="form-group" id="subcategories" style="display:none">
        </div>
      </div>

     <div class=" col-md-12  text-center">
      <br/>
        <button type="submit" class="btn bg-blue btn-flat ajax-submit">Save</button>
        <a href="{{ route('user.index') }}" class="btn bg-red btn-flat">Cancel</a>
     </div>
     {!! Form::close() !!}
    </div>
  </div>
</div>
</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
function add_parentcategory()
{
  $("#parentCategory").show(1000);
  $(".show-parentcat").hide(1000);
  $(".hide-parentcat").show(1000);
}
function hide_parentcategory()
{
  $("#parentCategory").hide(1000);
  $(".show-parentcat").show(1000);
  $(".hide-parentcat").hide(1000);
  $("#parent_category").val('');
}

      function get_subcategory()
       {

        cat_id=$("#parent_category").val();
        if(cat_id=="")
        {
          swal("Please select category");
          return;
        }
        count=$("#subcategories .sub_cat").length;
        var url = '{{ route("category.getsubcategory",array(":id",":count")) }}';
        url = url.replace(':id',cat_id);
        url=url.replace(':count',count);
        $.ajax({
          type: "GET",
          url: url,
          success: function(data) {
        $('#subcategories').append(data);
        $('#subcategories').show(1000);
        $('#parent_category_button').hide(1000);
        // $('#unit_topic').html(data);
          }
  });
       }

       function delete_sub_div_content()
       {
        $('#subcategories').html('');
        $('#subcategories').hide(1000);
         $('#parent_category_button').show(1000);
       }

      function get_sub_subcategory(id)
       {
       cat_id=$("#sub_category"+id).val();
       count=$("#subcategories .sub_cat").length;
      
       var url = '{{ route("category.getsubsubcategory",array(":id",":count")) }}';
       url = url.replace(':id',cat_id);
       url=url.replace(':count',count);

       $.ajax({
         type: "GET",
         url: url,
         success: function(data) {

        $('#subcategories').append(data);
        $('#sub_category_button'+id).hide(1000);
        // $('#unit_topic').html(data);
          }
        });

       }
      function delete_subdiv_contetnt(id)
       {
         newid=parseInt(id) + 1;

        $('#subCategory'+newid).html('');
        $("#subCategory"+newid).remove();
        $('#subCategory'+newid).hide(1000);
        $('#sub_category_button'+id).show(1000);
       }
</script>
<script>
$(document).ready(function(){
              $('form').submit(function(e){

               e.preventDefault();

               var me = $(this);
               var fd=new FormData($(this)[0]);

       // perform ajax
       $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
       }
        });


       $.ajax({

           url: me.attr('action'),

           type: 'post',

           data: fd,

           dataType: 'json',
           contentType: false,
           processData: false,

           success: function(response){

               if(response.success == 'true')
               {
                 swal("Category added successfully");
               //alert('category added successfully');
               location.reload();

                $("#form_error").hide();
            }

            else{

               $.each(response.responseJSON.errors, function(key, value) {

                     var element = $('#' + key);
                       element.closest('div.form-group')

                       .removeClass('.has-error')

                       .addClass(value.length > 0 ? 'has-error':'has-success' )

                       .find('.text-danger')

                       .remove();
                        element.after(value);

               });
           }
       },
       error:function(response){
        // alert(response.responseJSON.errors.category_name);
         //errors=console.log(response.responseJSON);
         $.each(response.responseJSON.errors, function(key, value) {

               var element = $('#' + key);
                 element.closest('div.form-group')

                 .removeClass('.has-error')

                 .addClass(value.length > 0 ? 'has-error':'has-success' )

                 .find('.text-danger')

                 .remove();
                  element.after('<p class="text text-danger">'+value+'</p>');

         });
       }
   });
   });
});
</script>
@endpush
