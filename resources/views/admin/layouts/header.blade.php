<header class="main-header">
    <!-- Logo -->
    <a href="{{ route('dashboard')}}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>MLM</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>MLM</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs">Admin</span>
            </a>
		  </li>
		  @if(isset(Auth::user()->name))
		  <li class="dropdown user user-menu">
            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
              <i class="fa fa-power-off"></i>
              <span class="hidden-xs">Sign out</span>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
		  </li>
		  @else
			<script>window.location = "{{ route('login')}}";</script>
		   @endif
        </ul>
      </div>
    </nav>
  </header>
