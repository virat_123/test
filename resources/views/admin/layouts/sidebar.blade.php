<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('public/lte/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin</p>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
		  <li class=""><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="treeview">
        <a href=""><i class="fa fa-users"></i>Customers
          <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('user.index')}}" class=""><i class="fa fa-list"></i>List Customers</a></li>
          <li><a href="{{route('user.create')}}" class=""><i class="fa fa-plus"></i>Add</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="">
          <i class="fa fa-truck"></i>Vendors
          <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('vendors.index')}}" class=""><i class="fa fa-list"></i>List Vendors</a></li>
          <li><a href="{{route('vendors.create')}}" class=""><i class="fa fa-plus"></i>Add</a></li>
         <!--  <li class="treeview">
            <a href="">
              <i class="fa fa-tags"></i>Category
              <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{route('category.index')}}" class=""><i class="fa fa-list"></i>List Categories</a></li>
              <li><a href="{{route('category.add')}}" class=""><i class="fa fa-plus"></i>Add</a></li>
            </ul>
          </li> -->
        </ul>
      </li>
      <li class="treeview">
        <a href="">
          <i class="fa fa-list-alt "></i>Category
          <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
              <li><a href="{{route('category.index')}}" class=""><i class="fa fa-list"></i>List Categories</a></li>
              <li><a href="{{route('category.add')}}" class=""><i class="fa fa-plus"></i>Add</a></li>
        </ul>
      </li>
        <li class="treeview">
        <a href="">
          <i class="fa fa-sitemap"></i>Levels
          <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
              <li><a href="{{route('levels.index')}}" class=""><i class="fa fa-list"></i>List Levels</a></li>
              <li><a href="{{route('levels.create')}}" class=""><i class="fa fa-plus"></i>Add</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href=""><i class="fa fa-book"></i>Transactions
          <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('transactions.create')}}" class=""><i class="fa fa-plus"></i>Add</a></li>
          <li><a href="{{route('transactions.index')}}" class=""><i class="fa fa-circle-o"></i>All Transaction</a></li>
          <li><a href="{{route('transactions.user_earnings')}}" class=""><i class="fa fa-circle-o"></i>User Earnings</a></li>
        </ul>
      </li>

      <li class="treeview">
        <a href=""><i class="fa fa-share"></i>Settings</a>
        <ul class="treeview-menu">
          <li><a href="{{route('user.level')}}" class=""><i class="fa fa-circle-o"></i>User Levels</a></li>
          <li><a href="#" class=""><i class="fa fa-circle-o"></i>Vendors</a></li>
        </ul>
      </li>
      <li class=""><a href=""><i class="fa fa-circle-o"></i>Transaction Report</a></li>

        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
