<!-- /.content-wrapper -->
<footer class="main-footer">
	<strong>Copyright &copy; {{ date('Y', time()) }}</strong>
</footer>

<!-- jQuery 3 -->
<script src="{{ asset('public/lte/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('public/lte/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('public/lte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Morris.js charts -->
<script src="{{ asset('public/lte/bower_components/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('public/lte/bower_components/morris.js/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('public/lte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('public/lte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('public/lte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('public/lte/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('public/lte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/lte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('public/lte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('public/lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('public/lte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('public/lte/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- public/lteLTE App -->
<script src="{{ asset('public/lte/dist/js/adminlte.min.js') }}"></script>
<!-- public/lteLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('public/lte/dist/js/pages/dashboard.js') }}"></script>
<!-- public/lteLTE for demo purposes -->
<script src="{{ asset('public/lte/dist/js/demo.js') }}"></script>
<!-- jQuery Form -->
<script src="{{asset('public/lte/plugins/jquery-form/jquery.form.js')}}"></script>
<!-- Sweet Alert -->
<script src="{{asset('public/lte/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('public/lte/plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{asset('public/lte/plugins/nestable/jquery.nestable.js')}}"></script>
@stack('scripts')
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
