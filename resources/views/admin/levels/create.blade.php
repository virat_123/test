@extends('admin.layouts.app')

@section('title', 'Levels')
@section('sub_title', 'Create Levels')
@section('content')
<div class="row">
   @if(Session::has('level_success'))
     <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                {{Session::get('level_success')}}
      </div>
    @endif
  	<div class="col-md-12">
		<div class="box box-primary">
			{!! Form::open(['route' => ['levels.store']]) !!}
            <div class="box-body">
                {{ csrf_field() }}
            	<div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('level_name') ? 'has-error' : ''}}">
                    		<label>Level Name</label>
            		  		{!! Form::text('level_name', null, array('placeholder' => 'Level Name','class' => 'form-control','id'=>'level_name')) !!}
                            {!! $errors->first('level_name', '<p class="help-block">:message</p>') !!}
                           
                    	</div>

                    </div>
                </div>
             <!--    <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('start_node') ? 'has-error' : ''}}">
                    		<label>Start Node</label>
            		  		{!! Form::text('start_node', null, array('placeholder' => 'Start Node','class' => 'form-control','id'=>'start_node')) !!}
                            {!! $errors->first('start_node', '<p class="help-block">:message</p>') !!}
                    	</div>
                    </div>   
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('end_node') ? 'has-error' : ''}}">
                    		<label>End Node</label>
            		  		{!! Form::text('end_node', null, array('placeholder' => 'End Node','class' => 'form-control','id'=>'end_node')) !!}
                            {!! $errors->first('end_node', '<p class="help-block">:message</p>') !!}
                    	</div>
                    </div>	
                </div> -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('level_percentage') ? 'has-error' : ''}}">
                           {!! Form::label('percetage', 'Percentage(%)') !!}
                            {!! Form::text('level_percentage', null, array('placeholder' => 'Level Percentage(%)','class' => 'form-control','id'=>'level_percentage')) !!}
                            {!! $errors->first('level_percentage', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>

                <div class="col-md-6  text-center">
                  <button type="submit" class="btn bg-blue btn-flat ajax-submit">Save</button>
                  <a href="" class="btn bg-red btn-flat">Cancel</a>
                </div>
            </div>

			{!! Form::close() !!}

		</div>
	</div>	
</div>

@endsection
@push('scripts')
@endpush