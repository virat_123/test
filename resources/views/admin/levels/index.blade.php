@extends('admin.layouts.app')

@section('title', 'Levels')
@section('sub_title', 'Levels Listing')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			{!! Form::open(['route' => ['level.calculate_earnings']]) !!}
			<div class="box-body">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                    		{!! Form::text('node','1 Node', array('placeholder' => '','class' => 'form-control','id'=>'','disabled')) !!} 
                    	</div>

                    </div>
                    <div class="col-md-2">
                  			{!! Form::text('node_value', 1000, array('placeholder' => 'Rupees','class' => 'form-control','id'=>'node_value','')) !!}
                            {!! $errors->first('node_value', '<p class="help-block">:message</p>') !!}
                	</div>

                	<div class="col-md-2">
                  			 <button type="submit" class="btn bg-blue btn-flat ajax-submit">Save</button>
                 			 <a href="" class="btn bg-red btn-flat">Cancel</a>
                	</div>
                </div>
            </div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
                <div class="box-header">
                    <a href="{{ route('levels.create') }}" class="btn bg-purple btn-flat pull-right"><i class="fa fa-plus"></i> Create New</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-striped table-responsive" id="example">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Percentage</th>
                                <th>Earnings</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@if(!empty($level))
                        	<?php 
                        	$total_earnings=0;
                        	?>
                        		@foreach($level as $l)
                        	<tr>
                        		<td>{{ $l['name'] }}</td>
                        		<td>{{ $l['percentage'] }}</td>
                        		<td>{{ $l['earnings'] }}</td>
                        		<?php
                        		$total_earnings+=$l['earnings'];
                        		?>
                        		<td>
                        			<a href="{{route('levels.show', $l['id'])}}" class="btn btn-sm bg-blue btn-flat" title="View User"><i class="fa fa-eye"></i></a>
                        			<a href="{{route('levels.edit', $l['id'])}}" class="btn btn-sm bg-green btn-flat" title="Edit User"><i class="fa fa-pencil"></i></a>
                        			<a href="#" id="{{ $l['id'] }} " data-src="{{route('levels.destroy', $l['id'])}}" class="btn btn-sm bg-red btn-flat delete" title="Delete User"><i class="fa fa-trash"></i></a>
                        		</td>
                        	</tr>
                        		@endforeach
                        		<tr>
                        			<td colspan="2">Total Earning
                        			</td>
                        			<td colspan="2">
                        				{{$total_earnings}}
                        			</td>
                        		</tr>
                        	@endif
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
	</div>
</div>
@endsection
@push('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

@endpush