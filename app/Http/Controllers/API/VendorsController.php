<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\State;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Categories;
use App\Roles;
use App\Vendor;
use App\Vendors_category;
use QRCode;
class VendorsController extends Controller
{
  public $successStatus = 200;

  
  public function get_vendors(Request $request)
  {
  	$input=$request->all();

  	$data['cat_id']= (isset($input['cat_id']) && !empty($input['cat_id']) ? $input['cat_id'] : null);
 
  	if($data['cat_id'] !=null)
  	{
      
  	$vendor=Categories::where('id',$data['cat_id'])->with('vendors')->first();
  	}
  	else
  	{
  		$vendor=User::with('category')->where('role_id',3)->get();
  	}
  	$response=['response'=>1,'category_vendor'=>$vendor];
  	return response()->json($response);
  	// SELECT id,name, ( 3959 * acos( cos( radians(22.31747) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(73.16063) ) + sin( radians(22.31747) ) * sin( radians( latitude ) ) ) ) AS distance FROM vendors HAVING distance < 25 ORDER BY distance LIMIT 0 , 20
  }

  public function scan_vendorcode(Request $request)
  {
    $input=$request->all();
    if(isset($input['vendor']) && !empty($input['vendor']))
    {
      $vendor_id = Crypt::decryptString($input['vendor']);
      $vendor=User::with('category')->where('id',$vendor_id)->get();
      $response=['response'=>1,'vendor'=>$vendor];
    }
    else
    {
      $response=['response'=>0,'msg'=>'vendor not found.'];

    }
    return response()->json($response);
  }

  

}