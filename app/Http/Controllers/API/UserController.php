<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Hash;
use App\User;
use App\Vendor;
use Illuminate\Support\Facades\Auth;
use Validator;
use QRCode;
class UserController extends Controller
{
  public $successStatus = 200;

  public function login(Request $request)
  {
    $input=$request->all();

     if($input['type']=='User')
     {
     //user login 
        if($input['provider']=='facebook' || $input['provider']=='google')
        {
            $checkemail = User::where(['email'=>$input['email'],'provider'=>'mail','role_id'=>2])->first();
            if($checkemail)
            {
            $response = ['response'=>0,'msg'=>'Email Already Taken'];
            // return response()->json($response);             
            } 
            else
            {
            $user = User::where(['email'=>$input['email'],'provider'=>$input['provider'],'role_id'=>2])->first();
                if($user)
                {

                    
                     Auth::login($user);
                       $user->access_token=$input['access_token'];
                      $user->save();
                     $success['user']=$user;
                    $success['token'] = $user->createToken('MyApp')-> accessToken;
                    $response=['data'=>$success,'response'=>1,'msg'=>'Login Successfully.'];
                    
                    //return response()->json(['success' => $success], $this-> successStatus);
                }
                else
                {
                    $input = $request->all();
                    unset($input['type']);
                    $input['role_id']=2;
                  //  $input['password'] = bcrypt($input['password']);
                    $user = User::create($input);
                    $file=public_path('generated_qrcodes/'.$user->id.'.png');
                    $encrypted = Crypt::encryptString($user->id);
                    $image=QRCode::text($encrypted)->setsize(4)->setMargin(2)->setOutfile($file)->png();
                    $user->qrcode_path=$user->id.'.png';
                    $user->save();
                    Auth::login($user);
                    $success['token'] =  $user->createToken('MyApp')-> accessToken;
                    $success['user']=$user;

                    //$success['name'] =  $user->name;
                    $response=['data'=>$success,'response'=>1,'msg'=>'Registered Successfully.'];
                }
            }
        }
        else
        {
      
            if(Auth::attempt(['email' => request('email'), 'password' => request('password'),'role_id'=>2]))
            {
            $success['user'] = Auth::user();
            $success['user']->access_token=$input['access_token'];
            $success['user']->save();
            $success['token'] =  $success['user']->createToken('MyApp')-> accessToken;
            $response=['data'=>$success,'response'=>1,'msg'=>'Login Successfully.'];
            //return response()->json(['success' => $success], $this-> successStatus);
             }
                
            else
            {
            $response=['response'=>0,'error'=>'Unauthorised'];
            $this-> successStatus=401;
            //return response()->json(['error'=>'Unauthorised'], 401);
            }
         

        }   

        return response()->json($response, $this-> successStatus);
    }
    else
    {
        //vendor login
        $vendor=User::where(['email'=>$input['email'],'role_id'=>3])->first();
        if($vendor)
        {
            Auth::login($vendor);
            $success['user']=$vendor;
            $success['token']=$vendor->createToken('MyApp')-> accessToken;
            $success['user']->access_token=$input['access_token'];
            $success['user']->save();
            $response=['data'=>$success,'response'=>1,'msg'=>'Login Successfully.'];
        }
        else
        {
             $response=['response'=>0,'error'=>'Unauthorised'];
            $this-> successStatus=401;
        }
        return response()->json($response, $this-> successStatus);
    }

    }

    public function register(Request $request)
    {
        $input = $request->all();
    
        $checkemail = User::where(['email'=>$input['email']])->first();
        if($checkemail)
        {
            $response = ['response'=>0,'msg'=>'Email Already Taken'];
            // return response()->json($response);             
        }
        else
        {
            $input = $request->all();
            $input['password'] = Hash::make($input['password']);
            $input['role_id']=2;
            $user = User::create($input);
            $file=public_path('generated_qrcodes/'.$user->id.'.png');
            $encrypted = Crypt::encryptString($user->id);
            $image=QRCode::text($encrypted)->setsize(4)->setMargin(2)->setOutfile($file)->png();
            $user->qrcode_path=$user->id.'.png';
            $user->save();
            Auth::login($user);
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            $success['user']=$user;
            //$success['name'] =  $user->name;
            $response=['data'=>$success,'response'=>1,'msg'=>'Registered Successfully.'];
            
        }
        return response()->json($response, $this-> successStatus);
    }

    public function details()
    {
       $user = Auth::user();
       return response()->json(['success' => $user], $this-> successStatus);
    }
}




 ?>
