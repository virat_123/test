<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\State;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Categories;
class CategoryController extends Controller
{
	public $successStatus = 200;


	public function get_parent_category()
	{
		$parent_categories=Categories::where('parent_id',0)->with(['vendors'=>function($query){return $query->limit(8); }])->take('8')->get();

		$response=['response'=>1,'categories'=>$parent_categories];
		return response()->json($response);
	}

	public function get_sub_category(Request $request)
	{
		$input=$request->all();
		if(isset($input['cat_id']) && !empty($input['cat_id']))
		{
			$categories=Categories::where('parent_id',$input['cat_id'])->get();
			$response=['response'=>1,'categories'=>$categories];
		}
		else
		{
			$response=['response'=>0,'msg'=>'Category Not Found.'];
		}
		return response()->json($response);
	}


}

?>