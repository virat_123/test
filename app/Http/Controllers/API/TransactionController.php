<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use App\Categories;
use App\Transaction;
use App\Roles;
use App\Vendor;
use App\Vendors_category;
use App\User;
use App\Traits\ApiTrait;
use QRCode;
use File;
use FCM;
use Image; 
class TransactionController extends Controller
{
	use ApiTrait;
 public $successStatus = 200;

 	public function user_transaction(Request $request)
 	{
 		/*
			Transaction status
			0=>pending
			1=>accept
			2=>rejected
			
 		 */
 		$input=$request->all();
 		if(isset($input) && !empty($input))
 		{
 			$data=array(
 						'amount'		=>$input['amount'],
 						'description'	=>$input['description'],
 						'user_id'		=>$input['user_id'],
 						'vendor_id'		=>$input['vendor_id'],
 						'status'		=>0
 					   );
	 		if($request->file('receipt'))
	 		{
	 			$file=$request->file('receipt');
	 			$filename=time().'.'.$file->getClientOriginalExtension();
	 			$destinationPath = 'public/uploaded_receipt';
	 			(string)Image::make($file)->save($destinationPath.'/'.$filename)->encode('png');
	 			$data['receipt']=$filename;
	 		}
	 		else
	 		{
	 			$data['receipt']="";
	 		}
	 		$transaction=Transaction::create($data);
	 		if($transaction)
	 		{
	 			$user=User::where('id',$input['user_id'])->first();
	 			//dd($user);

	 			$vendor=User::where('id',$input['vendor_id'])->first();
	 			$token=$vendor->access_token;
	 			// $data1=array();
	 			// $token="cgmwX8S_s_M:APA91bGykzVTolWw_2Q_i0G7eboN34ReArPrGARNDL_kFYQmxcbMnItXKewL2OcLo8yK3S4RzIHCupv0234J3r8P97k6kOiVOO0IdXGkqwHIZAuAEui_4RuGGf_COpmG82RjE72uWEby";
	 			$data1['title']="Transaction initiated.";
	 			$data1['body']=ucfirst($user->name)."created transaction";
	 			$data1['status']=0;
	 			$data1['data']=$transaction->id;
	 			$this->sendNotification($token,$data1);
	 		}
	 			$response = ['response'=>1,'msg'=>'Transaction Created Successfully'];

 		}
 		else
 		{
 				# code...
 			$response = ['response'=>0,'msg'=>'Transaction failed.'];
 		}	
 		return response()->json($response);
 	}

 	public function get_transaction_by_id(Request $request)
 	{
 		$input=$request->all();
 		if(isset($input['transaction_id']) && !empty($input['transaction_id']))
 		{	
 			$transaction=Transaction::where('id',$input['transaction_id'])->with('users')->get();
 			$response = ['response'=>1,'data'=>$transaction];
 		}
 		else
 		{
 			$response = ['response'=>0,'msg'=>'Transaction id not found.'];
 		}
 		return response()->json($response);
 	}


 	public function transaction_accept_reject(Request $request)
 	{
 		$input=$request->all();

 		if(isset($input['transaction_id']) && !empty($input['transaction_id']))
 		{
 			$transaction=Transaction::where('id',$input['transaction_id'])->with('vendors')->first();
 			if($input['status']==1)
 			{
 				//create nodes
 				$transaction->status=$input['status'];
 				// $transaction->save();
 				$transaction->save();

 				//send confirmation notification to user
 				$user=User::where('id',$transaction->user_id)->first();
 				$token=$user->access_token;
 				$data1['title']="Transaction accepted by vendor.";
	 			$data1['body']=ucfirst($transaction->name)."has accepted your transaction";
	 			$data1['status']=1;
	 			$data1['data']=$transaction->id;
	 			$this->sendNotification($token,$data1);
 				
				$response = ['response'=>1,'msg'=>'Transaction accepted successfully.','data'=>$transaction];
 			}
 			else
 			{
 				//transaction rejected

 				$transaction->status=$input['status'];
 				$transaction->reject_reason=$input['reject_reason'];
 				$transaction->save();
 				$user=User::where('id',$transaction->user_id)->first();
 				$token=$user->access_token;
 				$data1['title']="Transaction rejected by vendor.";
	 			$data1['body']=$transaction->reject_reason;
	 			$data1['status']=1;
	 			$data1['data']=$transaction->id;
	 			$this->sendNotification($token,$data1);

 				$response = ['response'=>1,'msg'=>'Transaction rejected.','data'=>$transaction];
 			}
 		}
 		else
 		{
 			$response = ['response'=>0,'msg'=>'Transaction id not found.'];
 		}
 		return response()->json($response);
 	}


 	public function vendor_transaction_history(Request $request)
 	{
 			$input=$request->all();
 			if(isset($input['vendor_id']) && !empty($input['vendor_id']))
 			{
 				$data['pending']=Transaction::where(['vendor_id'=>$input['vendor_id'],'status'=>0])->with('users')->get();
 				$data['accepted']=Transaction::where(['vendor_id'=>$input['vendor_id'],'status'=>1])->with('users')->get();
 				$data['rejected']=Transaction::where(['vendor_id'=>$input['vendor_id'],'status'=>2])->with('users')->get();
 			$response = ['response'=>1,'msg'=>'Request sent successfully.','data'=>$data];
 			}
 			else
 			{
				$response = ['response'=>0,'msg'=>'Vendor not found.'];
 			}
	 			return response()->json($response);
 	}
 	public function user_transaction_history(Request $request)
 	{
 			$input=$request->all();
 			if(isset($input['user_id']) && !empty($input['user_id']))
 			{
 				$data['transactions']=Transaction::where(['user_id'=>$input['user_id']])->with('vendors')->orderBy('id','desc')->get();
 				// $data['accepted']=Transaction::where(['user_id'=>$input['user_id'],'status'=>1])->with('vendors')->get();
 				// $data['rejected']=Transaction::where(['user_id'=>$input['user_id'],'status'=>2])->with('vendors')->get();
 			$response = ['response'=>1,'msg'=>'Request sent successfully.','data'=>$data];
 			}
 			else
 			{
				$response = ['response'=>0,'msg'=>'User not found.'];
 			}
	 		return response()->json($response);
 	}
}


?>