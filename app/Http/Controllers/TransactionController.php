<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendor;
use App\User;
use Auth;
use App\Transaction;
use App\Nodes;
use App\Wallet;
use App\Traits\ApiTrait;
class TransactionController extends Controller
{
  use ApiTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
        $transactions=Transaction::with('users')->with('vendors')->get();

        return view('admin.transactions.index',compact('transactions'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $vendors=User::where('role_id',3)->get()->toarray();
        return view('admin.transactions.create',compact('vendors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
          $this->validate($request,[
            'product_name'=>'required',
            'price'=>'required',
            'vendor'=>'required',
          ]);

          //$countnode=Nodes::where('status',0)->where('position',3)->count();

          //if($countnode >= 125)
          //{
           
           

          //}

          $data['user_id']=Auth::user()->id;
          $data['product_name']=$request->product_name;
          $data['amount']=$request->price;
          $data['vendor_id']=$request->vendor;

         Transaction::create($data);

          if($request->price >= 1000)
          {
                $amount=$request->price;
                $node=$amount/1000;
                $node=(int)$node;

                //active every node of user 
                $user=Auth::user();
                $last_node=Nodes::orderBy('id','desc')->first();
                if($last_node)
                {
                  $nodeid=$last_node->node_id;
                  $parent_id=$last_node->parent_id;
                  for($i=0;$i<$node;$i++)
                  {
                      if($nodeid+1  > 1 && $nodeid+1  <= 6)
                      {
                          $parent_id=1;
                          $parent_node=1;
                      }
                      if($nodeid+1  >= 7 && $nodeid+1  <= 31)
                      {
                          $parent_node=2;
                      }
                      if($nodeid+1 >31)
                      {
                          $parent_node=3;
                      } 
                      
                      $increment=$nodeid-1;
                      if($nodeid > 1 && $increment % 5 == 0)
                      {   
                          $parent_id++;
                      }

                      $array=array(
                                  'node_id'=>$nodeid+1,
                                  'user_id'=>$user->id,
                                  'level_id'=>0,
                                  'status'=>0,
                                  'position'=>$parent_node,
                                  'parent_id'=>$parent_id
                                  );
                        
                      $creatednode=Nodes::create($array);
                    
                     
                      $nodeid++;
                  }
                    $this->update_nodes();
                }
                else
                {
                    $parent_id=1;
                    for($i=1;$i<=$node;$i++)
                    {
                        if($i==1)
                        {
                            $parent_id=0;
                            $parent_node=0;
                        }
                        if($i >1 && $i <= 6)
                        {
                            $parent_id=1;
                            $parent_node=1;
                        }
                        if($i >=7 && $i<= 31)
                        {
                            $parent_node=2;
                        }
                        if($i>31)
                        {
                            $parent_node=3;
                        }

                        $array=array(
                                    'node_id'=>$i,
                                    'user_id'=>$user->id,
                                    'level_id'=>0,
                                    'status'=>0,
                                    'position'=>$parent_node,
                                    'parent_id'=>$parent_id
                                    );
                        Nodes::create($array);
                        $increment=$i-1;

                            if($i > 1 && $increment % 5==0)
                            {   
                                $parent_id++;
                            }   
                    }
                    $this->update_nodes();
                    //$this->update_node_level();


                }
        
                $rs=$amount%1000;

                if($rs > 0)
                {
                    $date=date('Y-m-d h:i:s');
                   

                    $wallet_exist=Wallet::where(['user_id'=>$user->id,'status'=>0])->first();
                    if($wallet_exist)
                    {
                        $balance=$rs+$wallet_exist->balance;
                        //logic if $balance is greater than 1000
                        $wallet_exist->balance=$balance;
                        $wallet_exist->status=0;
                        $wallet_exist->updated_at=$date;
                        //$wallet_exist->save();
                    }
                    else
                    {
                        $data=array(
                                    'user_id'=>$user->id,
                                    'balance'=>$rs,
                                    'description'=>'',
                                    'status'=>0,
                                    'created_at'=>$date
                                  );
                        Wallet::create($data);
                    }
                }
          }

            return redirect('transactions')
                        ->with('success','Transaction created successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function user_earning()
    {
        $user=Auth::user();
        $user_transaction=Transaction::where('user_id',$user->id)->get();
        $user_earning=Transaction::with('calculate_userearning')->get();
        return view('admin.transactions.usertransactions',compact('user_transaction'));
    }
}
