<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Hash;
use Auth;
use Datatables;
use App\User;
use App\Roles;
use App\Vendor;
use App\Vendors_category;
use Image;
use QRCode;

class VendorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         // $vendors=Vendor::with('category')->with('City')->get()->toArray();
         // echo "<pre>";
         // print_r($vendors);
         // exit;
       return view('admin.vendors.index');
    }


    public function data()
    {
      $vendors=User::with('category')->with('City')->where('role_id',3)->get();

          foreach ($vendors as $value)
          {
              if($value['status'] == 1)
              {
                 $value['status']='Active';
              }
              else
              {
                 $value['status']='InActive';
              }
          }

        return Datatables::of($vendors)
         ->rawColumns(['actions','status'])
          ->editColumn('category',function($vendors){
              $string='';
              if(sizeof($vendors->category) > 1)
              {
              foreach($vendors->category as $v)
              {
                  $string.=$v->name.' | ';
              }
            }
            else
            {
              foreach($vendors->category as $v)
              {
                  $string.=$v->name;
              }
            }
            
              
              return $string;
         })
         ->editColumn('status', function ($vendors) {
             if($vendors->status == 'Active'){
                 return '<label class="label label-success" >Active</label>';
             } else {
                 return '<label class="label label-danger">InActive</label>';
             }
         })
        
         ->addColumn('actions', function (User $vendors) {

          $view='<a href="'.route('vendors.show', $vendors->id).'" class="btn btn-sm bg-blue btn-flat" title="View User"><i class="fa fa-eye"></i></a>';
          $edit='<a href="'.route('vendors.edit', $vendors->id).'" class="btn btn-sm bg-green btn-flat" title="Edit User"><i class="fa fa-pencil"></i></a>';
          $delete='<a href="#" id="'. $vendors->id.'" data-src="'.route('vendors.destroy', $vendors->id).'" class="btn btn-sm bg-red btn-flat delete" title="Delete User"><i class="fa fa-trash"></i></a>';
         

             $action = $view.' '.$edit.' '.$delete;
             return $action;
         })
         ->make(true);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $states=DB::table('states')->where('country_id','101')->get()->toarray();
        $categories=DB::table('categories')->where('parent_id',0)->get();
        return view('admin.vendors.create',compact(['states','categories']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        // echo "<pre>";
        // print_r($request->all());
        // exit;

        $rules = [
          'name'            => 'required',
          'email'           => 'required|email|unique:vendors,email',
          'phone'           => 'required',
          'password'        => 'required|same:confirm_password',
          'state'           => 'required',
          'city'            => 'required',
          'pincode'         => 'required',
          'address'         => 'required',
          'percentage'      => 'required'
      ];
       $this->validate($request, $rules);

      $input = $request->all();
      $input["provider"]="";
      $input["provider_id"]=""; 
      $input["remember_token"]="";
      $input['status']=1;
      $input['password']=Hash::make($input['password']);

      unset($input['category']);
      unset($input['sub_category']);
      unset($input['confirm_password']);

       if($request->file('vendor_icon'))
      {
       $file=$request->file('vendor_icon');
        $filename=time().'.'.$file->getClientOriginalExtension();
        $destinationPath = 'public/vendor_icon';
       (string)Image::make($file)->save($destinationPath.'/'.$filename)->encode('png');
       //$file->move($destinationPath,$file->getClientOriginalName());
       //$file->move($destinationPath,$filename);
       //exit;
       $input['icon']=$filename;
      }
      else
      {
        $data['icon']=null;
      }
      unset($input['vendor_icon']);
      $vendor = User::create($input);

      if($vendor)
      {
        
      $file=public_path('generated_qrcodes/'.$vendor->id.'.png');
      $encrypted = Crypt::encryptString($vendor->id);
      $image=QRCode::text($encrypted)->setsize(4)->setMargin(2)->setOutfile($file)->png();
    
      
      $vendor->qrcode_path=$vendor->id.'.png';
      $vendor->save();

        if(isset($_POST['category']))
        {
            $data['categories_id']=$_POST['category'];
            Vendors_category::create($data);
           
            if(isset($_POST['sub_category']))
            {
                $subcategory=$_POST['sub_category'];
                foreach ($subcategory as $s) {
                   $data['categories_id']=$s;
                    Vendors_category::create($data);
                }
            }
        }
        $response=['success'=>true,'msg'=>'Vendor addedd Successfully.'];
        return response()->json($response);

      }
      else
      {
        $response=['success'=>false,'msg'=>'Something went wrong.'];
        return response()->json($response);
      }
      

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
