<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Userlevel;
use App\Vendor;
class Settings extends Controller
{
    //
    public function UserLevel()
    {
      $levels=UserLevel::get()->toarray();

      return view('admin.user_level',compact('levels'));
    }

    public function Vendors()
    {
      $vendors=Vendor::get()->toarray();

      return view('admin.vendors',compact('vendors'));
    }
}
