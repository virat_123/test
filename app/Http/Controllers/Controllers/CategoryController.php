<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Hash;
use Auth;
use Datatables;
use App\User;
use App\Roles;
use App\Categories;
use Image;  
class CategoryController extends Controller
{

  public function index()
  {
    
    $parent_categories=Categories::where('parent_id',0)->get();
     $i=0;
     foreach($parent_categories as $p)
     {
      $parent_categories[$i]['sub'] = $this->sub_categories($p['id']);
      $i++;
     }
     
    $data['menuview']=$this->fetch_menu($parent_categories);

        return view('admin.category.index',compact('data'));
  }

  public function sub_categories($id){
        $categories =Categories::where('parent_id',$id)->get();
        $i=0;
        foreach($categories as $p_cat){

            $categories[$i]['sub'] = $this->sub_categories($p_cat['id']);
            $i++;
        }
        return $categories;       
    }
    
    function fetch_menu($data){
  ob_start();
  echo '<ol class="dd-list">';
  
  $i=1;
  foreach($data as $menu){
    echo '<li class="dd-item" data-id="'.$menu["id"].'">
      <div class="pull-right">
      <button type="button" class="btn btn-circle" onclick="update_category_modal('.$menu["id"].')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Category Name"><i class="fa fa-pencil text-primary"></i></button>
                        <button type="button" onclick="delete1('.$menu["id"].')" class="btn btn-circle"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Category"><i class="fa fa-times text-danger"></i> </button>
                        </div>
      <div class="dd-handle">'.$menu["name"].'
      
                        </div>
                        ';
      if(!empty($menu['sub'])){

      echo '<ol class="dd-list">';

      $this->fetch_sub_menu($menu['sub']);

      echo '</ol>';
      }
      
      
      echo '</li>';

    
  $i++;
  }
  echo '</ol>';
   $content_post=ob_get_clean();
    return $content_post;

}

function fetch_sub_menu($sub_menu){

  $i=1;
  foreach($sub_menu as $menu){

    echo '<li class="dd-item" data-id="'.$menu["id"].'">
    <div class="pull-right">
      <button type="button" class="btn btn-circle" onclick="update_category_modal('.$menu["id"].')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Category Name"><i class="fa fa-pencil text-primary"></i></button>
                        <button type="button" onclick="delete1('.$menu["id"].')" class="btn btn-circle"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Category"><i class="fa fa-times text-danger"></i> </button>
                        </div>
      <div class="dd-handle">'.$menu["name"].'</div>';
    
    if(!empty($menu['sub'])){

      echo '<ol class="dd-list">';

      $this->fetch_sub_menu($menu['sub']);

      echo '</ol>';
      }
      
      
      echo '</li>'; 
  $i++;
  }
  
  

}
  public function add()
  {
    $parent_categories=Categories::where('parent_id',0)->get();
    return view('admin.category.create',compact('parent_categories'));
  }

  public function get_sub_category($id,$count)
  {
    $sub_categories=Categories::where('parent_id',$id)->get()->toarray();

    $num_of_sub=$count;
    if(!empty($sub_categories))
    {
  ?>

      <div class="col-md-10">
        <br/>
          <select name="sub_category[]" id="sub_category<?php echo $num_of_sub; ?>" onchange="delete_subdiv_contetnt('<?php echo $num_of_sub; ?>')" class="form-control sub_cat">
            <?php foreach($sub_categories as $s): ?>
              <option value="<?php echo $s['id']; ?>"><?php echo $s['name']; ?></option>
            <?php  endforeach; ?>
          </select>
      </div>
      <div class="col-md-2">
              <br/>
          <button type="button" id="sub_category_button<?php echo $num_of_sub; ?>" onclick="get_sub_subcategory('<?php echo $num_of_sub; ?>')" class="btn bg-blue btn-flat"><i class="fa fa-plus"></i></button>
      </div>

  <?php
    }
    else
    {
    ?>
    <div class="col-md-10">
      <br/>
    <p class="text-danger">No Sub Category Available. </p>
   </div>
    <?php
      }
    ?>
    </div>
<?php
  }

  public function get_sub_subcategory($id,$count)
    {
     $sub_categories=Categories::where('parent_id',$id)->get()->toarray();
      $num_of_sub=$count;
 ?>
    <div id="subCategory<?php echo $num_of_sub; ?>" class="sub_cat">
  <?php
    if(!empty($sub_categories))
    {
      $div_id=sizeof($sub_categories);
    ?>
        <div class="col-md-10">
          <br/>
            <select name="sub_category[]" id="sub_category<?php echo $num_of_sub; ?>" onchange="delete_subdiv_contetnt('<?php echo $num_of_sub; ?>')" class="form-control">
              <?php
              foreach($sub_categories as $s):
              ?>
              <option value="<?php echo $s['id']; ?>"><?php echo $s['name']; ?></option>
              <?php
              endforeach;
              ?>
            </select>
        </div>
        <div class="col-md-2">
          <br/>
          <button type="button" id="sub_category_button<?php echo $num_of_sub; ?>" onclick="get_sub_subcategory(<?php echo $num_of_sub; ?>)" class="btn bg-blue btn-flat"><i class="fa fa-plus"></i></button>
        </div>
          <?php
              }
            else
              {
            ?>
            <div class="col-md-10">
              <br/>
              <p class="text-danger">No Sub Category Available. </p>
            </div>
          <?php
              }
           ?>
    </div>
<?php
    }

  public function store(Request $request)
  {
    
     $rules = [
              'category_name'=> 'required'
              ];
      $this->validate($request,$rules);

    $input=$request->all();
 
    if(isset($input['parent_category']) && !empty($input['parent_category']))
    {
        if(isset($input['sub_category']))
        {
        end($input['sub_category']);
         $key = key($input['sub_category']);
         $sub_array=$input['sub_category'];
         $sub_value=$sub_array[$key];
         $data=array(
             'name'=>$input['category_name'],
             'parent_id'=>$sub_value,
             );
        }
      else {
        // code...
        $data=array(
          'name'=>$input['category_name'],
          'parent_id'=>$input['parent_category']
        );

      }

  
      if($request->file('category_icon'))
      {
       $file=$request->file('category_icon');
       $filename=time().'.'.$file->getClientOriginalExtension();
       $destinationPath = 'public/category_icon';
       (string)Image::make($file)->save($destinationPath.'/'.$filename)->encode('png');
       //$file->move($destinationPath,$file->getClientOriginalName());
       //$file->move($destinationPath,$filename);
       //exit;
       $data['icon']=$filename;
      }
      else
      {
        $data['icon']=null;
      }

      Categories::create($data);
    }
    else
    {
       
      $data=array(
        'name'=>$input['category_name'],
        'parent_id'=>0
      );

      if($request->file('category_icon'))
      {
        $file=$request->file('category_icon');
        $filename=time().'.'.$file->getClientOriginalExtension();
        $destinationPath = 'public/category_icon';
       (string)Image::make($file)->save($destinationPath.'/'.$filename)->encode('png');
       //$file->move($destinationPath,$file->getClientOriginalName());
       //$file->move($destinationPath,$filename);
       //exit;
       $data['icon']=$filename;
      }
      else
      {
        $data['icon']=null;
      }
          
        Categories::create($data);
    }
     return response()->json(['success'=>'true']);

  }


  public function updateorder()
  {
      $category_list=$_GET['Category_list'];
    
    $category_array=json_decode($category_list);
    $list=$this->update_parent($category_array);
   
   return response()->json(['success'=>'true']);

  }

  function update_parent($data){
  //echo "<pre>";
  //print_r($data);
 // exit;
  foreach($data as $menu)
  {
  
      $data=array(
      'parent_id'=>'0'
      );

      Categories::where('id',$menu->id)->update($data);

      if(isset($menu->children) && !empty($menu->children)){

        $this->update_child($menu->children,$menu->id);
    
      }
  }
}

function update_child($sub_menu,$parent_menu_id)
{

  foreach($sub_menu as $menu)
  {
      $data=array(
        'parent_id'=>$parent_menu_id
        );
      Categories::where('id',$menu->id)->update($data);
     
      if(isset($menu->children) && !empty($menu->children))
      {
       $this->update_child($menu->children,$menu->id);
      }
    }
  }

}
