<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\levels;
class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $level=Levels::get()->toArray();
        return view('admin.levels.index',compact('level'));
    }

    public function data()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('admin.levels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate([
        'level_name' => 'required|max:255',
        'level_percentage' => 'required',
    ]);
        // 'start_node' => 'required',
        // 'end_node'   => 'required',
        // 'start_node'=>$input['start_node'],
        //  'end_node'=>$input['end_node'],
        $input=$request->all();
        $data=array(
                    'name'=>$input['level_name'],
                    'percentage'=>$input['level_percentage'],
                    );
        Levels::create($data);
        return redirect('levels/create')->with('level_success','Level created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function calculate_earnings(Request $request)
    {
         $validatedData = $request->validate([
        'node_value' => 'required|integer',   
             ]);
         $node_value=$request->node_value;

         $levels=Levels::orderby('id','asc')->get()->toArray();  

           for($i=0;$i<sizeof($levels);$i++)
           {
            $previous_level_earnings=0;
                //current level earning
                $p_n_earning=$node_value*1/100;
                if($i==0)
                {
                    $node=pow(1,1);
                }
                else if($i==1)
                {
                    $node=pow(5,1);
                }
                else
                {
                    $node=pow(5,$i);
                }
                $current_level_earning=$node*$p_n_earning;
                // echo $current_level_earning."</br>";
               
                //previous level earning
                if($i > 0)
                {
                    $k=0;
                    $previous_level_earnings=0;
                   
                    for($j=$i;$j>0;$j--)
                    {
                        $p_n_earning1=$node_value*$levels[$j]['percentage']/100;
                        if($k==0)
                        {
                            $node=pow(1,1);
                        }
                        else if($k==1)
                        {
                            $node=pow(5,1);
                        }
                        else
                        {
                            $node=pow(5,$k);
                        }
                        
                        $previous_level_earnings+=$node*$p_n_earning1;
                        $k++;
                    }
                }
             

                 $total_earning=$current_level_earning+$previous_level_earnings;
                 
                 $data=array('earnings'=>$total_earning);

                 Levels::where('id',$levels[$i]['id'])->update($data);

            }
              return redirect('levels')->with('earning_success','Noode settings updated successfully');
           
    }
}
