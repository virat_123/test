<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Hash;
use Auth;
use Datatables;
use App\User;
use App\Roles;
use QRCode;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.users.index');
    }


    public function data()
    {

      $users = User::where('role_id','2')->get();

          foreach ($users as $value)
          {
              if($value['status'] == 1)
              {
                 $value['status']='Active';
              }
              else
              {
                 $value['status']='InActive';
              }
          }

        return Datatables::of($users)
         ->rawColumns(['actions','status'])
         ->editColumn('status', function ($users) {
             if($users->status == 'Active'){
                 return '<label class="label label-success" >Active</label>';
             } else {
                 return '<label class="label label-danger">InActive</label>';
             }
         })
         ->addColumn('actions', function (User $users) {

          $view='<a href="'.route('user.show', $users->id).'" class="btn btn-sm bg-blue btn-flat" title="View User"><i class="fa fa-eye"></i></a>';
          $edit='<a href="'.route('user.edit', $users->id).'" class="btn btn-sm bg-green btn-flat" title="Edit User"><i class="fa fa-pencil"></i></a>';
          $delete=($users->user_type != "admin") ? '<a href="#" id="'. $users->id.'" data-src="'.route('user.destroy', $users->id).'" class="btn btn-sm bg-red btn-flat delete" title="Delete User"><i class="fa fa-trash"></i></a>' : '';
         $change_password = (Auth::user()->user_type == "admin") ? '<a href="'.route('users.changepassword', $users->id).'" class="btn btn-sm bg-yellow btn-flat" title="Change Password"><i class="fa fa-key"></i></a>' : '';

             $action = $view.' '.$edit.' '.$delete.' '.$change_password;
             return $action;
         })
         ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $states=DB::table('states')->where('country_id','101')->get();
        return view('admin.users.create',compact('states'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        //
        $rules = [
          'name'            => 'required',
          'email'           => 'required|email|unique:users,email',
          'phone'           => 'required',
          'password'        => 'required|same:c_password',
          'state'           => 'required',
          'city'            => 'required',
          'pincode'         => 'required',
          'address'         => 'required',
          'status'          => 'required'
      ];
      $this->validate($request, $rules);

      $input = $request->all();
      unset($input['c_password']);
      $input["provider"]="";
      $input["provider_id"]="";
      $input['status']=1;
      $input['password']=Hash::make($input['password']);

      $user = User::create($input);
      
      $file=public_path('generated_qrcodes/'.$user->id.'.png');
      $encrypted = Crypt::encryptString($user->id);
      $image=QRCode::text($encrypted)->setsize(4)->setMargin(2)->setOutfile($file)->png();
    
      
      $user->qrcode_path=$file;
      $user->save();
    //$decrypted = Crypt::decryptString($encrypted);
    //echo $decryp;
   // exit;
      return redirect(route('user.create'))->with('msg','User Created Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getCity($id) {

        $states = DB::table("cities")->where("state_id",$id)->pluck("name","id");
        return json_encode($states);

    }
}
