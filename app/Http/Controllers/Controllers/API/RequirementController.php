<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Vendor;
use App\State;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Reject_reason;
class RequirementController extends Controller
{
  public $successStatus = 200;


  public function getstate()
  {
  	$states=DB::table('states')->where('country_id','101')->get();
  	 return response()->json(['states' => $states], $this-> successStatus);
  }

  public function getcity(Request $request)
  {
  		$input = $request->all();
  		if(!empty($input) && !empty($input['state_id']))
  		{
  			$id=$input['state_id'];
  			$city = DB::table("cities")->where("state_id",$id)->get();
  			//$data['city']= State::find($id)->cities();
  			$response= ['response'=>1,'city'=>$city];

  		}
  		else
  		{
  			$response=['response'=>0,'msg'=>"State id not found."];

  		}
  		return response()->json($response);

  }
  // public function login()
  // {
  //       if(Auth::attempt(['email' => request('email'), 'password' => request('password')]))
  //       {
  //           $user = Auth::user();
  //           $success['token'] =  $user->createToken('MyApp')-> accessToken;
  //           return response()->json(['success' => $success], $this-> successStatus);
  //       }
  //       else
  //       {
  //           return response()->json(['error'=>'Unauthorised'], 401);
  //       }
  //   }

  //   public function register(Request $request)
  //   {
  //       $validator = Validator::make($request->all(), [
  //           'name' => 'required',
  //           'email' => 'required|email',
  //           'password' => 'required',
  //           'c_password' => 'required|same:password'
  //       ]);
  //         if ($validator->fails())
  //           {
  //             return response()->json(['error'=>$validator->errors()], 401);
  //           }
  //           $input = $request->all();
  //           $input['password'] = bcrypt($input['password']);
  //           $user = User::create($input);
  //           $success['token'] =  $user->createToken('MyApp')-> accessToken;
  //           $success['name'] =  $user->name;
  //           return response()->json(['success'=>$success], $this-> successStatus);
  //   }

  //   public function details()
  //   {
  //      $user = Auth::user();
  //      return response()->json(['success' => $user], $this-> successStatus);
  //   }


    public function get_category(Request $request)
    {

    }


    public function search(Request $request)
    {
        $input=$request->all();
        if(isset($input) && !empty($input['search']))
        {
          $search=$input['search'];
          $data=User::with('Category')->where('name', 'like', '%'.$search.'%')->where('role_id',3)->get();
          $response= ['response'=>1,'data'=>$data];
        }
        else
        {
          $response=['response'=>0,'msg'=>"Data not found."];
        }
        return response()->json($response);
    }


    public function reject_reason()
    {
      $reject_reason=Reject_reason::get();
      $response= ['response'=>1,'data'=>$reject_reason];
      return response()->json($response);
    }
}




 ?>
