<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
      return $this->hasOne('App\Roles','role_id');
    }


    public function nodes()
    {
        return $this->hasMany('App/Nodes','user_id');
    }

       public function City()
       {
        return $this->belongsTo('App\Cities','city');
       }

         public function category()
       {
        return $this->belongsTomany('App\categories','vendor_category');
       }
}
