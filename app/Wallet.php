<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    //
	protected $table="user_wallet";

	protected $guarded=['id'];


	public function user_wallet()
	{
		return  $this->belongsTo('App\user','user_id');
	}
}
