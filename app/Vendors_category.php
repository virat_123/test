<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendors_category extends Model
{
    //
      protected $table = 'vendor_category';
       protected $guarded = ['id'];


       	public function vendor()
 		{
 			return $this->hasMany('App\User');
 		}

 		public function category()
 		{
 			return $this->hasMany('App\Category');
 		}

 		
}
