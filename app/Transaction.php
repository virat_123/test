<?php

namespace App;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //
    protected $table = 'transactions';
    protected $guarded=['id'];
    protected $fillable=['amount','receipt','reject_reason','vendor_id','user_id','description','status'];

    public function users()
   	{
   		return $this->belongsTo('App\User','user_id');
   	}

   	public function vendors()
   	{
   		return $this->belongsTo('App\User','vendor_id');
   	}

    public function calculate_userearning()
    {
      $user=Auth::user();
      $userstransaction=DB::table('transactions')->where('user_id',$user->id)->sum('amount');
      $currentnode=$userstransaction/1000;

      echo $currentnode;
      exit;
    }
}
