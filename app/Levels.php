<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Levels extends Model
{
    //
    protected $table="levels";
    protected $guarded=['id'];
    protected $fillable=['name','start_node','end_node','percentage'];
}
