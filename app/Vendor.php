<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    //
      protected $table = 'vendors';

       protected $guarded = ['id'];


       public function category()
       {
       	return $this->belongsTomany('App\categories','vendor_category');
       }

       public function City()
       {
       	return $this->belongsTo('App\Cities','city');
       }
}
