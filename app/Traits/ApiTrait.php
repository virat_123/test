<?php 

namespace App\Traits;

use App\Nodes;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

trait ApiTrait{


	public function update_node_level()
	{
		$currentnode1=Nodes::where('position',0)->where('status',0)->first();

		for($i=$currentnode1->node_id;$i<$currentnode1->node_id+124;$i++)
		{

			$currentnode=Nodes::where('node_id',$i)->first();
			$getchild_count=$this->getchild_count($i);
			
			if(sizeof($getchild_count)==5)
			{
				$currentnode->level_id=1;
				$currentnode->save();
						
						//check floors I.e no of nodes
				 		$node_count=$this->get_nodes_from_position($position=2);
				 		if($node_count>0 && $node_count == pow(5,2))
				 		{
				 			$currentnode->level_id=2;
				 			$currentnode->save();
				 		}

				 		$node_count=$this->get_nodes_from_position($position=3);

				 		if($node_count > 0 && $node_count == pow(5,3))
				 		{
				 			Nodes::where('node_id',$i)->update(['level_id'=>3,'status'=>1,'position'=>4]);
				 			$this->reorder_nodes($i);
				 		}
			}

			
		}
	}

	public function getchild_count($id)
	{
		return $childs=Nodes::where('parent_id',$id)->get()->toarray();
	}

	public function get_nodes_from_position($position)
	{
			return $count=Nodes::where('position',$position)->count();
	}

	public function reorder_nodes($id)
	{
		$lastnode=Nodes::orderBy('node_id','desc')->where('status',0)->first();
		$j=0;
		$level=0;
		for($i=$id+1;$i<=$lastnode->node_id;$i++)
		{
			if($i==$id+1)
			{
				Nodes::where('node_id',$i)->update(['position'=>$level]);
				$level++;
			}	
			else
			{
				if($j==6)
				{
					$level++;
				}
				if($j==31)
				{
					$level++;
				}
				if($j>156)
				{
					$level=3;
				}
				Nodes::where('node_id',$i)->update(['position'=>$level]);
			}


			$j++;
		}
	}




	public function update_nodes()
	{
		$level_1_check=Nodes::where('position',1)->count();
		if($level_1_check==5)
		{
			$update_data=['level_id'=>1];
			Nodes::where('position',0)->update($update_data);
		}
		
		$level_2_check=Nodes::where('position',2)->count();
		if($level_2_check==25)
		{
			$update_data=['level_id'=>2];
			Nodes::where('position',0)->update($update_data);
			$update_data=['level_id'=>1];
			Nodes::where('position',1)->update($update_data);
		}

		$level_3_check=Nodes::where('position',3)->count();
		
		if($level_3_check>=125)
		{
			$update_data=['level_id'=>1];
			Nodes::where('position',2)->update($update_data);

			$update_data=['level_id'=>2];
			Nodes::where('position',1)->update($update_data);

			$update_data=['level_id'=>3,'status'=>1,'position'=>4];
			Nodes::where('position',0)->update($update_data);
			$node=Nodes::where('position',4)->get()->toarray();
			end($node);         // move the internal pointer to the end of the array
			$key = key($node);
			$this->reorder_nodes($node[$key]['node_id']);
			
		}
	}

	public function sendNotification($tokens,$data)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($data['title']);
        $notificationBuilder->setBody($data['body'])->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
       
        // $temp=array();
        // $temp['notification']=$data;
        // $temp['body']=$data['body'];
        // $temp['title']=$data['title'];
        // $temp['status']=$data['status'];
      
    
        $dataBuilder->addData($data);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $datas = $dataBuilder->build();
  
        // You must change it to get your tokens
        $tokens = $tokens;

        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $datas);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        //return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        $downstreamResponse->tokensToModify();

        //return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();

        // return Array (key:token, value:errror) - in production you should remove from your database the tokens present in this array
        $downstreamResponse->tokensWithError();     
    }

    public function singleNotification()
    {
     $optionBuilder = new OptionsBuilder();
     $optionBuilder->setTimeToLive(60*20);

     $notificationBuilder = new PayloadNotificationBuilder('my title');
     $notificationBuilder->setBody('Hello world')
                         ->setSound('default');

     $dataBuilder = new PayloadDataBuilder();
     $dataBuilder->addData(['a_data' => 'my_data']);

     $option = $optionBuilder->build();
     $notification = $notificationBuilder->build();
     $data = $dataBuilder->build();

     $token = $data['token'];

     $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

     $downstreamResponse->numberSuccess();
     $downstreamResponse->numberFailure();
     $downstreamResponse->numberModification();

     //return Array - you must remove all this tokens in your database
     // $downstreamResponse->tokensToDelete();

     //return Array (key : oldToken, value : new token - you must change the token in your database )
     $downstreamResponse->tokensToModify();

     //return Array - you should try to resend the message to the tokens in the array
     // $downstreamResponse->tokensToRetry();                
    }

}