<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::get('getState','API\RequirementController@getstate');
Route::get('getCity','API\RequirementController@getcity');
Route::post('search','API\RequirementController@search');
Route::post('getSubCategory','API\CategoryController@get_sub_category');
Route::get('getVendors','API\VendorsController@get_vendors');
Route::get('getreject_reason','API\RequirementController@reject_reason');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middleware'=>'auth:api'],function(){
Route::get('getCategory','API\CategoryController@get_parent_category');
Route::post('details','API\UserController@details');
Route::post('scan_vendor','API\VendorsController@scan_vendorcode');
Route::post('user/create_transaction','API\TransactionController@user_transaction');
Route::post('vendor/transaction_by_id','API\TransactionController@get_transaction_by_id');
Route::post('vendor/transaction/accept_reject','API\TransactionController@transaction_accept_reject');
Route::post('vendor/transaction_history','API\TransactionController@vendor_transaction_history');	
Route::post('user/transaction_history','API\TransactionController@user_transaction_history');

//Route::post('search','API\RequirementController@search');
//Route::post('getVendors','API\VendorsController@get_vendors');

});
