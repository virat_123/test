<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// use QRCode;

Route::get('/','DashboardController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => 'auth'], function(){

Route::get('/Dashboard','DashboardController@index')->name('dashboard');

Route::get('/User-Levels','Settings@UserLevel')->name('user.level');
//Route::get('/Vendors','Settings@Vendors')->name('vendors');
Route::resource('transactions', 'TransactionController');
Route::get('transaction/users','TransactionController@user_earning')->name('transactions.user_earnings');
//user routes
Route::resource('/user', 'UsersController');
Route::get('users/data','UsersController@data')->name('user.data');
Route::get('city/get/{id}', 'UsersController@getCity')->name('get.city');

//vendor routes
Route::resource('/vendors', 'VendorsController');
Route::get('vendor/data','VendorsController@data')->name('vendor.data');

//category routes
Route::get('category/add','CategoryController@add')->name('category.add');
Route::get('category/view','CategoryController@index')->name('category.index');
Route::post('category/store','CategoryController@store')->name('category.store');
Route::get('category/get_sub_category/{id}/{count}','CategoryController@get_sub_category')->name('category.getsubcategory');
Route::get('category/get_sub_subcategory/{id}/{count}','CategoryController@get_sub_subcategory')->name('category.getsubsubcategory');
Route::get('category/update_order','CategoryController@updateorder')->name('category.updateorder');

Route::resource('levels', 'LevelController');
Route::post('level/calculate_earnings','LevelController@calculate_earnings')->name('level.calculate_earnings');

// Route::get('qr-code', function () 
// {		
// 	$file=public_path('generated_qrcodes/1.png');
//   	$encrypted = Crypt::encryptString('Hello world.');
//   	echo $encrypted;
//   	$decrypted = Crypt::decryptString($encrypted);
//   	echo $decryp;
//   	exit;
//   	$image=QRCode::text($encrypted)->setsize(4)->setMargin(2)->setOutfile($file)->png();    
//   	return $image;
// });
});
